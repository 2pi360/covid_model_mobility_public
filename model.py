from math import log, exp, inf

import numba
from numpy import array, zeros, convolve

from prob_prog import Model, derived_quantity, prior, basic_transformation, likelihood_factor

# Data
from read_data import T_d, T_w  # number of days and weeks
from read_data import POP_SIZES  # Population sizes in four countries
from read_data import HOSP  # number of hospitalizations per week per country
from read_data import MD_LONG, MD_SHORT  # raw mobility matrices for long+term travellers and commuters

# Distribution of time to outcome
from extra_data import HOSP_DISTR  # time from infection to hospitalization


def prior_for_Ro(Ro_w):
    '''
    log prior for a Gaussian random walk, used for weekly values of R.
    The first element of the sequence is ~Normal(2, 1/2)
    The subsequent elements changes by ~Normal(0, 1/4)
    All values should be positive
    '''
    if any(x<0 for x in Ro_w):
        return float('-inf')
    return - ((Ro_w[0]-2)**2 * 2) - sum((a-b)**2 * 8 for a, b in zip(Ro_w, Ro_w[1:]))


def smooth_weekly_into_daily(Ro_w):
    '''
    Interpolate weekly numbers into daily numbers
    '''
    Ro_d = [(a*(7-i) + b*i) / 7 for a, b in zip(Ro_w, Ro_w[1:]) for i in range(7)]
    Ro_d = [Ro_w[0]]*7 + Ro_d + [Ro_w[-1]]*7
    Ro_d = Ro_d[:T_d]
    return Ro_d


# Detection Factor to scales the numbers of inflow infections
# See appendix for the conceptual details of the reporting factor
INFLOW_FACTOR = [0.00759042, 0.00690119, 0.00666447, 0.00672377, 0.00697261,
                 0.00733497, 0.00775811, 0.00820976, 0.00865486, 0.00907331,
                 0.00944982, 0.00977164, 0.01002545, 0.0101999 , 0.01030032,
                 0.01033791, 0.01031327, 0.01023002, 0.01009173, 0.00989789,
                 0.00965303, 0.00936686, 0.0090659 , 0.00875303, 0.00843389,
                 0.00811247, 0.00778729, 0.00746112, 0.0071402 , 0.00684387,
                 0.00657058, 0.00632325, 0.00609812, 0.00589304, 0.00570872,
                 0.00554562, 0.00540747, 0.00529379, 0.00520567, 0.00514277,
                 0.00510434, 0.0050911 , 0.00510402, 0.00513625, 0.00518852,
                 0.00526342, 0.00536195, 0.00548591, 0.00563579, 0.00581436,
                 0.00600478, 0.00620859, 0.00642719, 0.00666035, 0.00691042,
                 0.00717359, 0.00745206, 0.00773272, 0.00801687, 0.00830345,
                 0.00859527, 0.00889159, 0.00919044, 0.00947736, 0.00974906,
                 0.01001238, 0.01026837, 0.01052503, 0.01078696, 0.01102062,
                 0.01124393, 0.01146393, 0.0116707 , 0.01187643, 0.01209248,
                 0.01231895, 0.01250097, 0.0126778 , 0.01285746, 0.01302249,
                 0.01320199, 0.01340304, 0.01359155, 0.01370982, 0.01381993,
                 0.01393927, 0.01403786, 0.01413183, 0.01422732, 0.0143368 ,
                 0.01439512, 0.01443872, 0.01448407, 0.01450921, 0.01451648,
                 0.0145218 , 0.01455589, 0.01455659, 0.014544  , 0.01452842,
                 0.01449044, 0.01446923, 0.01442589, 0.01442201, 0.0143603 ,
                 0.01431233, 0.01426831, 0.01421139, 0.01413125, 0.0140579 ,
                 0.01402316, 0.01400923, 0.0139013 , 0.01383686, 0.01378027,
                 0.01371991, 0.0136675 , 0.01367595, 0.01367951, 0.01370459,
                 0.01375246, 0.01380422, 0.01386889, 0.01395095, 0.01407558,
                 0.01419752, 0.01434063, 0.01450581, 0.01467984, 0.01484115,
                 0.01510433, 0.01539128, 0.01562438, 0.01588103, 0.016136  ,
                 0.01637472, 0.01657673, 0.01675291, 0.01694928, 0.01712194,
                 0.0172825 , 0.01740202, 0.01737969, 0.01732116, 0.0172298 ,
                 0.01712619, 0.01700522, 0.01687767, 0.01673263, 0.01657347,
                 0.01640442, 0.01623709, 0.01608743, 0.01595656, 0.01585681,
                 0.01578922, 0.01575139, 0.01575474, 0.01579909, 0.01589623,
                 0.01604002, 0.01624006, 0.01650352, 0.01682685, 0.01721143,
                 0.01765139, 0.01813055, 0.01866108, 0.01924725, 0.01988641,
                 0.02058009, 0.02132419, 0.02211103, 0.02294514, 0.02380131,
                 0.02469643, 0.0256013 , 0.02650256, 0.02739955, 0.0282801 ,
                 0.02914388, 0.02997043, 0.03077467, 0.03151736, 0.03219648,
                 0.03281382, 0.0333593 , 0.03386355, 0.03430481, 0.03469207,
                 0.0350048 , 0.03525436, 0.03545179, 0.03560854, 0.03574106,
                 0.03585297, 0.03595971, 0.03606084, 0.03616894, 0.03629521,
                 0.03644672, 0.03662833, 0.0368383 , 0.03708691, 0.03737996,
                 0.03771035, 0.03807939, 0.03848619, 0.03891429, 0.03935781,
                 0.03982597, 0.04031938, 0.04082791, 0.04135631, 0.04189635,
                 0.0424141 , 0.04291156, 0.04342356, 0.0439352 , 0.04442849,
                 0.04492545, 0.0454178 , 0.045859  , 0.04626467, 0.04669546,
                 0.04710016, 0.04746115, 0.04780795, 0.04813014, 0.04838112,
                 0.04858435, 0.04881333, 0.04899746, 0.04912265, 0.04924133,
                 0.04933777, 0.04937804, 0.049384  , 0.04942279, 0.0494405 ,
                 0.04943274, 0.04943884, 0.04946274, 0.0494771 , 0.04951003,
                 0.04960413, 0.04971427, 0.04984554, 0.05002235, 0.05023794,
                 0.05044573, 0.05068418, 0.05099467, 0.05133291, 0.05167482,
                 0.05203959, 0.05240735, 0.05274784, 0.05305849, 0.05334861,
                 0.05361934, 0.05385491, 0.05406362, 0.05423366, 0.05434226,
                 0.05439662, 0.05439638, 0.05434854, 0.05425185, 0.05411642,
                 0.05394172, 0.05371928, 0.05345195, 0.05315183, 0.05283244,
                 0.05249913, 0.05216255, 0.05182717, 0.05148989, 0.05115213,
                 0.05082441, 0.05048555, 0.05014191, 0.04980553, 0.0494803 ,
                 0.04916582, 0.04886069, 0.04857431, 0.04829011, 0.04801424,
                 0.04775462, 0.04751577, 0.04729513, 0.04708614, 0.04689875,
                 0.0467107 , 0.04652486, 0.04634664, 0.04617598, 0.04600657,
                 0.04582719, 0.04564804, 0.04546648, 0.04527841, 0.04508917,
                 0.04489551, 0.04469038, 0.04445793, 0.04421347, 0.04393871,
                 0.04363086, 0.04329593, 0.04293485, 0.04254044, 0.04210367,
                 0.04163927, 0.04114021, 0.04061016, 0.040079  , 0.03953052,
                 0.03893778, 0.03830846]


class EpidemicModel(Model):

    #########
    ## R_t ##
    #########

    # one R_t vector for each country
    Ro_fin_w: prior_for_Ro = [1.2]*T_w  # Ro per week
    Ro_swe_w: prior_for_Ro = [1.2]*T_w
    Ro_nor_w: prior_for_Ro = [1.2]*T_w
    Ro_den_w: prior_for_Ro = [1.2]*T_w

    @derived_quantity
    def Ro_rt(Ro_fin_w, Ro_swe_w, Ro_nor_w, Ro_den_w):
        ''' Convert four vectors of weekly R_t into a single array of daily R_t '''
        Ro_fin_d = smooth_weekly_into_daily(Ro_fin_w)
        Ro_swe_d = smooth_weekly_into_daily(Ro_swe_w)
        Ro_nor_d = smooth_weekly_into_daily(Ro_nor_w)
        Ro_den_d = smooth_weekly_into_daily(Ro_den_w)
        return array([Ro_fin_d, Ro_swe_d, Ro_nor_d, Ro_den_d])


    #################
    ##   Mobility  ##
    #################

    @derived_quantity
    def inflow_t():
        ''' Numbers of inflow infections, coming from outside Nordics '''
        inflow = zeros((T_d, 4))
        for j, b in enumerate(('FIN', 'SWE', 'NOR', 'DEN')):
            # here our source data is multiplied by the factor of 4.5,
            # we divide by 4.5 to get the original value
            inflow[:, j] = MD_LONG[f'OTHER_2_{b}_in'] / 4.5 / INFLOW_FACTOR
        return inflow

    @derived_quantity
    def M_long_out_t():
        ''' Portion of the populations leaving from Nordics '''
        dispersion = zeros((T_d, 4))
        for j, b in enumerate(('FIN', 'SWE', 'NOR', 'DEN')):
            dispersion[:, j] = MD_LONG[f'{b}_2_OTHER'] / POP_SIZES[j]
        return dispersion

    @derived_quantity
    def D_short_t():
        ''' Numbers of people commuting between Nordics '''
        dispersion = zeros((T_d, 4, 4))
        for i, a in enumerate(('FIN', 'SWE', 'NOR', 'DEN')):
            for j, b in enumerate(('FIN', 'SWE', 'NOR', 'DEN')):
                if a != b:
                    dispersion[:, j, i] = MD_SHORT[f'{a}_2_{b}']
        return dispersion

    @derived_quantity
    def D_long_t():
        ''' Numbers of people doing long-duration travels between Nordics '''
        dispersion = zeros((T_d, 4, 4))
        for i, a in enumerate(('FIN', 'SWE', 'NOR', 'DEN')):
            for j, b in enumerate(('FIN', 'SWE', 'NOR', 'DEN')):
                if a != b:
                    dispersion[:, j, i] = MD_LONG[f'{a}_2_{b}']
        return dispersion

    @derived_quantity
    def M_short_t(D_short_t):
        ''' Convert the D matrix into M matrix for the commuters. See equation (7) for details  '''
        dispersion = D_short_t.copy()
        for i in range(4):
            dispersion[:, :, i] = dispersion[:, :, i] / POP_SIZES[i] / 2
            dispersion[:, i, i] = 1 - sum(dispersion[:, j, i] for j in range(4) if j != i)
        return dispersion

    @derived_quantity
    def M_long_t(D_long_t):
        ''' Convert the D matrix into M matrix for the long-duration travellers. See equation (6) for details '''
        dispersion = D_long_t.copy()
        for i in range(4):
            dispersion[:, :, i] = dispersion[:, :, i] / POP_SIZES[i]
            dispersion[:, i, i] = 1 - sum(dispersion[:, j, i] for j in range(4) if j != i)
        return dispersion


    ###########################
    ##       SEIR model      ##
    ###########################

    @derived_quantity
    def SIR_model(Ro_rt, M_short_t, M_long_t, M_long_out_t, inflow_t) -> 'S I R new_I':
        """
        Simulate the hidden state of the epidemic (true numbers of infected) with a given set of parameters.

        Ro_rt: R per country, per day
        M_short_t: portion of commuters per population size per day
        M_long_t: portion of long duration travellers per population size per day
        M_long_out_t: portion of population leaving Nordics per day, per country
        inflow_t: number of infection arriving to the Nordics per day, per country
        """
        # Average duration of the infectious period
        I_dur = 8

        # init arrays
        I_rd = zeros((4, T_d))      # infectious
        S_rd = zeros((4, T_d))      # susceptible
        R_rd = zeros((4, T_d))      # removed
        new_I_rd = zeros((4, T_d))  # new potentially hospitalized infections

        # intitial states
        N = zeros(4, float) + POP_SIZES
        R_rd[:, 0] = R = zeros(4, float)
        I_rd[:, 0] = I = N / 100_000
        S_rd[:, 0] = S = N  - I

        new_infection = zeros(4, float)
        new_I = zeros(4, float)
        new_S = zeros(4, float)
        new_R = zeros(4, float)

        # Run the SIR mmodel
        for t in range(1, T_d):
            # infection pressure, equation (5)
            pressure = [Ro_rt[r, t] * sum(M_short_t[t, r, :] * I) / N[r] / I_dur    for r in range(4)]

            for r in range(4):
                # equation (4)
                new_infection[r] = S[r] * sum(M_short_t[t, :, r] * pressure)

                # equations (1-3)
                new_S[r] = sum(M_long_t[t, r, :] * S)  -  new_infection[r]
                new_I[r] = sum(M_long_t[t, r, :] * I)  +  new_infection[r]  +  inflow_t[t, r]  -  I[r]*M_long_out_t[t, r]  -  I[r]/I_dur
                new_R[r] = sum(M_long_t[t, r, :] * R)                                                                      +  I[r]/I_dur

            S[:] = new_S
            I[:] = new_I
            R[:] = new_R
            N = S + I + R

            # fix potential problems
            S[S<0] = 0

            # save values
            I_rd[:, t] = I
            S_rd[:, t] = S
            R_rd[:, t] = R
            new_I_rd[:, t] = new_infection

        return S_rd, I_rd, R_rd, new_I_rd



    @derived_quantity
    def metrics(Ro_rt, M_short_t, M_long_t, D_short_t, D_long_t, M_long_out_t, inflow_t):
        """
        This function implement the same equation as the one, but also keeps lots of additional metrics.
        See equations (11 - 19)
        """
        # pre-set parameters
        I_dur = 8

        # init arrays
        N = zeros(4, float) + POP_SIZES
        I_rd = zeros((4, T_d))      # infectious
        S_rd = zeros((4, T_d))      # susceptible
        R_rd = zeros((4, T_d))      # removed
        new_I_rd = zeros((4, T_d))  # new potentially hospitalized infections

        # intitial states
        N = zeros(4, float) + POP_SIZES
        R_rd[:, 0] = R = zeros(4, float)
        I_rd[:, 0] = I = N / 100_000
        S_rd[:, 0] = S = N  - I

        new_infection = zeros(4, float)
        new_I = zeros(4, float)
        new_S = zeros(4, float)
        new_R = zeros(4, float)

        metrics = {name: zeros((4, T_d)) for name in 'BCEFGHIN'}
        metrics['delta'] = zeros((4, T_d))
        metrics['total effect'] = zeros((4, T_d))
        metrics['total commuters effect'] = zeros((4, T_d))
        metrics['long prevalence'] = zeros((4, T_d))
        metrics['short prevalence'] = zeros((4, T_d))

        for t in range(T_d):
            # infection pressure
            active_infections =  [sum(M_short_t[t, r, :] * I)   for r in range(4)]
            pressure = [Ro_rt[r, t] * active_infections[r] / N[r] / I_dur    for r in range(4)]

            for r in range(4):
                new_infection[r] = S[r] * sum(M_short_t[t, :, r] * pressure)
                new_S[r] = sum(M_long_t[t, r, :] * S)  -  new_infection[r]
                new_I[r] = sum(M_long_t[t, r, :] * I)  +  new_infection[r]  +  inflow_t[t, r]  -  I[r]*M_long_out_t[t, r]  -  I[r]/I_dur
                new_R[r] = sum(M_long_t[t, r, :] * R)                                                                      +  I[r]/I_dur

            # save values
            for x in range(4):
                ## equation (11)
                # we dont need metric A, as it is already recorded as new_I
                metrics['B'][x, t] = sum(M_long_t[t, x, y] * I[y] for y in range(4) if y != x)
                metrics['C'][x, t] = sum(M_long_t[t, y, x] * I[x] for y in range(4) if y != x)
                # we dont need metric D, as it is already recorded as inflow_t
                metrics['E'][x, t] = I[x]*M_long_out_t[t, x]
                metrics['delta'][x, t] = new_infection[x] + metrics['B'][x, t] - metrics['C'][x, t] + inflow_t[t, x] - metrics['E'][x, t]

                # check everything is correct
                assert abs(I[x]*(1-1/I_dur) +  metrics['delta'][x, t]   -   new_I[x]) < 0.0001


                ## equation (13)
                metrics['F'][x, t] = Ro_rt[x, t] / N[x] / I_dur * S[x] * I[x]
                metrics['G'][x, t] = metrics['F'][x, t] * (1-M_short_t[t, x, x]**2)
                metrics['H'][x, t] = Ro_rt[x, t] / N[x] / I_dur * (S[x] * M_short_t[t, x, x]) * sum(M_short_t[t, x, y] * I[y] for y in range(4) if y != x)
                metrics['I'][x, t] = sum(Ro_rt[y, t] / N[y] / I_dur * (S[x] * M_short_t[t, y, x]) * active_infections[y] for y in range(4) if y != x)
                metrics['total commuters effect'][x, t] = - metrics['G'][x, t] + metrics['H'][x, t] + metrics['I'][x, t]

                # check everything is correct
                assert abs(metrics['F'][x, t] + metrics['total commuters effect'][x, t]  -   new_infection[x]) < 0.0001

                ## equation (14)
                metrics['total effect'][x, t] = metrics['total commuters effect'][x, t] + metrics['B'][x, t] - metrics['C'][x, t] + inflow_t[t, x] - metrics['E'][x, t]

                ## equations 18 and 19
                metrics['long prevalence'][x, t] =   sum(D_long_t[t, y, x] / N[y] * I[y] for y in range(4) if y != x)  / sum(D_long_t[t, y, x] for y in range(4) if y != x)
                metrics['short prevalence'][x, t] =  sum(D_short_t[t, y, x] / N[y] * I[y] for y in range(4) if y != x) / sum(D_short_t[t, y, x] for y in range(4) if y != x)

            S[:] = new_S
            I[:] = new_I
            R[:] = new_R
            N = S + I + R
            metrics['N'][:, t] = N

            # fix potential problems
            S[S<0] = 0

        return metrics

    @derived_quantity
    def effective_R(Ro_rt, metrics, I):
        """
        Effective reproduction number, i.e.
        the average number of secondary infections caused by a single infected individual
        """
        ## equation (16)
        return metrics['F'] / I * 8

    @derived_quantity
    def effective_M(metrics, I):
        """
        Effective multiplication number, i.e.
        the number of new infections that emerged in country x for any reason
        during the average infectious period, divided by the number of new infections
        """
        ## equation (17)
        return metrics['delta'] / I * 8

    #######################
    ## Observation model ##
    #######################

    @derived_quantity
    def expected_hosp(new_I):
        ''' convolutions of infections, used to compute expected cases. Equation (8). '''
        expected = zeros((4, T_w))
        for r in range(4):
            expected[r, :] = convolve(new_I[r], HOSP_DISTR)[:T_d].reshape(-1, 7).sum(1)
        return expected * 0.01


    @likelihood_factor
    def likelihood_hospitalization(expected_hosp):
        ''' Equation (9)  '''
        return (
            dl_negabinom_up_to_c(HOSP[0, :].astype(int), expected_hosp[0, :], 0.1).sum() +    # FIN
            dl_negabinom_up_to_c(HOSP[1, 7:].astype(int), expected_hosp[1, 7:], 0.1).sum() +  # SWE (contains some missing data)
            dl_negabinom_up_to_c(HOSP[2, :].astype(int), expected_hosp[2, :], 0.1).sum() +    # NOR
            dl_negabinom_up_to_c(HOSP[3, :].astype(int), expected_hosp[3, :], 0.1).sum()      # DEN
            )


# this function is vectorized using numba, so they can be applied to arrays and matrixes
@numba.vectorize([numba.float64(numba.int32, numba.float64, numba.float64)])
def dl_negabinom_up_to_c(x, mean, over):
    """
    PDF of the Negative Binomial (aka overdispersed poisson aka Polya) distribution, defined with mean and overdispersion, up to a constant.
    Assumes mean > 0, ignores the constant.

    The expression is derived as:

        nbinom.pmf(x) = choose(x+f-1, f-1) * p**x * (1-p)**f
        f = 1 / over
        p = mean / (mean+f)

        log(nbinom.pmf(x)) =
            x*log(p) + f*log(1-p) + lgamma(x+f) - lgamma(f) - lgamma(x+1)
            or
            x*log(mean) - (x+f)*log(mean+f) + f*log(f) + lgamma(x+f) - lgamma(f) - lgamma(x+1)

        terms depending only on x and f are ingored (as data and overdispersion is considered to be constant)
    """
    f = 1 / over
    #p = mean / (mean+f)
    if x == 0:
        return -f*log(mean+f)
    else:
        return x*log(mean) - (x+f)*log(mean+f)


if __name__ == '__main__':
    # some tests
    epidemic = EpidemicModel()
    print(epidemic)
    print(epidemic.Likelihood)
    print(epidemic.I.sum(0))
    print(epidemic.new_I.sum(0))
    print(epidemic.new_I.sum(1))
    print(epidemic.all_priors())
    print(epidemic.all_likelihood_factors())
    print(epidemic.metrics)
