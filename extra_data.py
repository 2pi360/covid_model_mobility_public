import useful
from numpy import array, exp


def nb_distr(mean, std):
    """
    negative binomial distribution
    """
    assert std**2 > mean
    over = (std**2 - mean) / mean**2
    # count NB(x) for x = 1...99
    distr = [exp(useful.dlpolya(t, mean=mean, over=over)) for t in range(100)]
    # cut the tail
    distr = [distr[i] for i in range(100) if sum(distr[:i]) < 0.9999]
    # normalized the rest of the values, just in case
    distr = array(distr) / sum(distr)
    return distr


# time from infection to hospitalization
HOSP_DISTR = nb_distr(mean=11, std=5)


if __name__ == '__main__':
    print(HOSP_DISTR)
    print(len(HOSP_DISTR))
