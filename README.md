This is a code, used for the paper
The influence of cross-border mobility on the COVID-19 epidemic in Nordic countries




# LIST OF FILES:

MCMC.py
- main file. Running it will start the inference, which creates the output file with posterior samples

model.py
- describes the epidemic model

prob_prog.py
- provides Model class, which allows to define models with a declarative language.

ram.py
- adaptive protocol for MCMC by Matti Vihola.

read_data.py
- reads data from a file

extra_data.py
- Creates a distribution

useful.py
- collection of different utility functions

data_pers.py
- utility functions for reading and writing MCMC samples

- hospitalizations.csv
number of hospitalizations per country per week

- mobility_extra.csv
Mobility data


# VISUALIZATION

Due to limitations, parts of the code responsible for visualizing the results were not provided.
Only a demo of the visualization code is provided.


# INSTRUCTIONS

Run MCMC.py

The MCMC samples would be saved into 'C:/history/covid_modelling' folder, or any other folder defined by TRIAL_N in MCMC.py line 11.
The inference can take a day, but the first (warm-up) samples should be available immediately
and could be read/analysed.

To read, analyse and visualize the produces samples, see visualization.ipynb (jupyted notebook file).
Alternatevely, one can run preview_samples.py to see that samples exists.
Samples are loaded as a pandas.DataFrame object, to save them into a different format see pandas.DataFrame documantaion.

Sometimes a warning appears: "RuntimeWarning". It is produces by numpy, as MCMC tries to samples from invadid regions of the parameter space.
This should not cause any problems.


# REQUIREMENTS

Code was tested on

```
Python 3.6.1

numpy==1.12.1
scipy==0.19.0
pandas==0.20.1
numba==0.33.0
```
