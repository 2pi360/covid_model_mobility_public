import datetime
import pandas
from numpy import array, zeros, nan, isnan


## MODELLED PERIOD
# length of the modelled, in days and weeks
T_w = 46
T_d = T_w * 7
# modelled dates
# forst modelled day is 10 Feb 2020, Monday
MODELLED_DAYS = [datetime.date(2020, 2, 10) + datetime.timedelta(days=t) for t in range(T_d)]
MODELLED_WEEKS = MODELLED_DAYS[::7]


## POPULATION SIZES
# Order of Countries in this code
# Finland   Sweden   Norway   Denmark

# population sizes per country in 2020, taken from google
POP_SIZES = [5_531_000, 10_350_000, 5_379_000, 5_831_000]


## LOADING HOSPITALZTION DATA
HOSP = pandas.read_csv('hospitalizations.csv')
HOSP = array([HOSP['FIN'], HOSP['SWE'], HOSP['NOR'], HOSP['DEN']])
# discard all data after the end of the modelled period
HOSP = HOSP[:, :T_w]


## LOADING MOBILITY DATA
mobility_dateset = pandas.read_csv('mobility_extra.csv')

cols = """
   FIN_2_SWE FIN_2_NOR FIN_2_DEN
   SWE_2_FIN SWE_2_NOR SWE_2_DEN
   NOR_2_FIN NOR_2_SWE NOR_2_DEN
   DEN_2_FIN DEN_2_SWE DEN_2_NOR
   FIN_2_OTHER NOR_2_OTHER DEN_2_OTHER SWE_2_OTHER
   OTHER_2_FIN OTHER_2_NOR OTHER_2_DEN OTHER_2_SWE
   OTHER_2_FIN_in OTHER_2_NOR_in OTHER_2_DEN_in OTHER_2_SWE_in""".split()

start  = list(mobility_dateset.date).index('2020-02-10')

commuters = mobility_dateset[[c+'_com' for c in cols]]
commuters.columns = cols
MD_SHORT = commuters.iloc[start:start+T_d].reset_index()


longduration = mobility_dateset[[c+'_emi' for c in cols]]
longduration.columns = cols
MD_LONG = longduration.iloc[start:start+T_d].reset_index()

# we will need the data from the previous year for counterfactual modelling
old_start  = list(mobility_dateset.date).index('2019-02-10')
MD_OLD_SHORT = commuters.iloc[old_start:old_start+T_d].reset_index()
MD_OLD_LONG = longduration.iloc[old_start:old_start+T_d].reset_index()


if __name__ == '__main__':
    # some tests
    print('-'*80)
    print('FIN SWE NOR DEN')

    for line in zip(MODELLED_WEEKS[::],
                    HOSP[0],
                    HOSP[1],
                    HOSP[2],
                    HOSP[3]
                    ):
        print(*line, sep='\t')

    print('-'*80)
